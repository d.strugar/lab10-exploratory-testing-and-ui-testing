import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

class TestGoogleScholar {

    @Test
    public void testGoogleScholar() {
        WebDriver driver = new FirefoxDriver();
        driver.get("https://scholar.google.com/");
        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals("Google Академик", driver.getTitle());

        WebElement findLine = driver.findElement(By.xpath("//*[@id=\"gs_hdr_tsi\"]"));
        findLine.sendKeys("covid19 pandemic");

        WebElement searchButton = driver.findElement(By.xpath("//*[@id=\"gs_hdr_tsb\"]"));
        searchButton.click();

        WebElement didYouMeanButton = driver.findElement(By.xpath("/html/body/div/div[10]/div[2]/div[2]/div[1]/div/a"));
        didYouMeanButton.click();

        // assert that title has changed
        findLine = driver.findElement(By.xpath("//*[@id=\"gs_hdr_tsi\"]"));
        Assert.assertEquals("covid 19 pandemic", findLine.getAttribute("value"));

        // lets go to second page of search results
        WebElement secondPage = driver.findElement(By.xpath("/html/body/div/div[10]/div[2]/div[2]/div[3]/div[2]/center/table/tbody/tr/td[3]/a"));
        secondPage.click();

        // lets go forward one page of results
        WebElement nextPage = driver.findElement(By.xpath("/html/body/div/div[10]/div[2]/div[2]/div[3]/div[2]/center/table/tbody/tr/td[12]/a"));
        nextPage.click();

        // now we should be on third page
        // this works because if active it is wrapped in <b> tag, otherwise it is not
        WebElement thirdPageBold = driver.findElement(By.xpath("/html/body/div/div[10]/div[2]/div[2]/div[3]/div[2]/center/table/tbody/tr/td[4]/b"));
        Assert.assertEquals("3", thirdPageBold.getText());


        // now sort by date
        WebElement sortByDate = driver.findElement(By.xpath("/html/body/div/div[10]/div[1]/div/ul[1]/li[2]/a"));
        sortByDate.click();

        // assert that third is now red
        WebElement sortedByDateLabel = driver.findElement(By.xpath("/html/body/div/div[10]/div[1]/div/ul[1]/li[2]/a"));
        Assert.assertEquals("rgb(209, 72, 54)", sortedByDateLabel.getCssValue("color"));

        driver.quit();
    }
}
