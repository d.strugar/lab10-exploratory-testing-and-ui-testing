# Landmark Tours (Use Cases)

Website tested: https://scholar.google.com/

## Use Case #1 (implemented)

| Question                      | Answer                                                     |
|-------------------------------|------------------------------------------------------------|
| Test tour                     | NO1                                                        |
| Object to be tested           | Find COVID19 related articles                              |
| Test duration                 | 9.117 s                                                    |
| Tester                        | Dragos Strugar                                             |
| Further Testing Opportunities | Instead of "covid19 pandemic" use different search strings |

## Use Case #2 (implemented)

| Question                      | Answer                                                                                                              |
|-------------------------------|---------------------------------------------------------------------------------------------------------------------|
| Test tour                     | NO2                                                                                                                 |
| Object to be tested           | Find more articles, not just ones on the first page                                                                 |
| Test duration                 | 12.311 s                                                                                                            |
| Tester                        | Dragos Strugar                                                                                                      |
| Further Testing Opportunities | Go more than two levels deep - maybe go through 10 levels and include potential patents and details about vaccines. |

Use Case #3 (implemented)

| Question                      | Answer                                                                                                                                               |
|-------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------|
| Test tour                     | NO3                                                                                                                                                  |
| Object to be tested           | Different sorting filters                                                                                                                            |
| Test duration                 | 9.902 s                                                                                                                                              |
| Tester                        | Dragos Strugar                                                                                                                                       |
| Further Testing Opportunities | Include only papers published in 2021. Maybe also include 2020. Also a possibility is to include only papers from 2019, there are not many of those. |

# Protocol (combining all use cases)

| No. | What is done                                                                                            | Status | Comment                                                                                          |
|-----|---------------------------------------------------------------------------------------------------------|--------|--------------------------------------------------------------------------------------------------|
| 1   | Search for "covid19 pandemic" in search box                                                             | OK     | Got results, but Google suggests to use "covid 19 pandemic"                                      |
| 2   | Google's suggestion was useful, so use that instead of own query, i.e. click on "did you mean" button   | OK     | Got results, now everything is OK                                                                |
| 3   | Looking at papers, but want to see more. Clicked a pagination button to take me to page "2".            | OK     | Taken me to another page with brand new papers and studies.                                      |
| 4   | Satisfied with my results, but want to explore a bit more. Clicked on "Next" button to take me further. | OK     | System correctly took me to page 3, which is a successor to page 2.                              |
| 5   | Now I want to sort results by date (newest). I find the button dedicated for that and I click it.       | OK     | System refreshes the results so it prioritises study publish time over other relevance criteria. |

